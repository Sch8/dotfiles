## Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd nomatch notify
unsetopt beep extendedglob
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '~/.zshrc'
 
autoload -Uz compinit
compinit
# End of lines added by compinstall

source ~/.antigen.zsh
antigen init ~/.antigenrc

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs history time)
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_MODE='nerdfont-complete'
DEFAULT_USER=kerkmann

#zsh shortcuts
alias discord='Discord'

alias gc='git yolo -a'
alias gp='git push'
alias gcp='git yolo -a; git push'

alias la='ls -ahl'
alias pinentry='pinentry-gtk-2'

alias hie='hie-8.8.3'

alias nsp='ns-usbloader -g ver=v0.8'
alias nspa='ns-usbloader -g ver=v0.8 *'
alias mega='megadl ' \
    '-u ${pass show nz/mega/Mega | grep login: | sed -E "s/login: //g"}' \
    '-p ${pass show nz/mega/Mega | head -n }'

#alias vim='nvim'

#alias sudo='sudo -E'
#alias svim='sudo -E nvim'

#eval "$(dircolors ~/.dircolors)";

export EDITOR=nvim
#export PATH=$PATH:/opt/piavpn/bin:~/bin
#
#
#export R_LIBS_USER="$HOME/.r-packages"
#
#export JDK_HOME="/usr/lib/jvm/default-runtime"
#export JAVA_HOME="$JDK_HOME"
#export PATH="$PATH:$HOME/.gem/ruby/2.7.0/bin:$HOME/.npm-packages/bin:$HOME/.local/bin:$HOME/.pub-cache/bin"
#export PATH="$PATH:$HOME/.SpaceVim.d/lsp/kotlin-language-server/server/build/scripts"
#export MANPATH="${MANPATH-$(manpath)}:$HOME/.npm-packages/share/man"

#[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
