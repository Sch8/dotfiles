autocmd BufDelete * if empty(filter(tabpagebuflist(), '!buflisted(v:val)')) | Startify | endif
"autocmd VimEnter * Startify
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd w
