" Window switching
" Copyright SpaceVim start
" https://github.com/SpaceVim/SpaceVim/blob/0ba60675cd9d64ef412bfec2ded037d15e9a6cb1/autoload/SpaceVim/layers/core/statusline.vim#L707
function! s:windowJump(i) abort
  if winnr('$') >= a:i
    exe a:i . 'wincmd w'
  endif
endfunction
" Copyright SpaceVim end

nmap <silent><leader> :WhichKey '<Space>'<CR>
nmap <silent><leader>      :<c-u>WhichKey '<Space>'<CR>
nmap <silent><localleader> :<c-u>WhichKey  ','<CR>
vmap <silent><leader> :<c-u>WhichKeyVisual '<Space>'<CR>

" TODO
nmap <leader>!s :split<CR>:wincmd w<CR>
nmap <leader>!t <leader>!s<CR>:terminal

" Applications
nmap <leader>as :Startify<CR>

" Buffers
nmap <leader><leader><S-TAB> :bprevious<CR>
nmap <leader><leader><TAB> :bnext<CR>
nmap <leader><leader>1 1gt
nmap <leader><leader>2 2gt
nmap <leader><leader>3 3gt
nmap <leader><leader>4 4gt
nmap <leader><leader>5 5gt
nmap <leader><leader>6 6gt
nmap <leader><leader>7 7gt
nmap <leader><leader>8 8gt
nmap <leader><leader>9 9gt

nmap <leader>bh :%!xxd<CR>
nmap <leader>bH :%!xxd -r<CR>

" Gradle
nmap <leader>gb :w <BAR> !gradle build<CR>
nmap <leader>gc :w <BAR> !gradle check<CR>
nmap <leader>gr :w <BAR> !gradle run<CR>
nmap <leader>gR :w <BAR> !gradle bootRun<CR>
nmap <leader>gc :w <BAR> !gradle test<CR>

" Header
nmap <leader>hcA :AddApacheLicense<CR>
nmap <leader>hca :AddAGPLicense<CR>
nmap <leader>hcg :AddGNULicense<CR>
nmap <leader>hce :AddEUPLicense<CR>
nmap <leader>hcm :AddMITLicense<CR>

" Language
"function! s:show_documentation()
"  if (index(['vim','help'], &filetype) >= 0)
"    execute 'h '.expand('<cword>')
"  else
"    call CocAction('doHover')
"  endif
"endfunction
"nnoremap <silent> K :call <SID>show_documentation()<CR>
"nmap <silent><leader>lR :<C-u>CocListResume<CR>
"nmap <silent><leader>ld :call CocAction('doHover')<CR>
"nmap <silent><leader>led :CocList diagnostics<CR>
"nmap <silent><leader>lE <Plug>(coc-fix-current)
"nmap <silent><leader>lgd <Plug>(coc-definition)
"nmap <silent><leader>lgi <Plug>(coc-implementation)
"nmap <silent><leader>lgr <Plug>(coc-references)
"nmap <silent><leader>lgt <Plug>(coc-type-definition)
"nmap <silent><leader>lo :<C-u>CocList outline<cr>
"nmap <silent><leader>lp <Plug>(coc-previous)
"nmap <silent><leader>ls :<C-u>CocList -I symbols<CR>
"vmap <silent><leader>lv <Plug>(coc-codeaction-selected)
nmap <silent><leader>lD :<C-u>CocFzfList diagnostics --current-buf<CR>
nmap <silent><leader>la :CocCommand actions.open<CR>
nmap <silent><leader>lc :<C-u>CocFzfList commands<CR>
nmap <silent><leader>ld :<C-u>CocFzfList diagnostics<CR>
nmap <silent><leader>le :<C-u>CocFzfList extensions<CR>
nmap <silent><leader>len <Plug>(coc-diagnostic-next)
nmap <silent><leader>lep <Plug>(coc-diagnostic-prev)
nmap <silent><leader>lf <Plug>(coc-format)
nmap <silent><leader>li :<C-u>CocFzfList<CR>
nmap <silent><leader>ll :<C-u>CocFzfList location<CR>
nmap <silent><leader>lo :<C-u>CocFzfList outline<CR>
nmap <silent><leader>lp :<C-u>CocFzfListResume<CR>
nmap <silent><leader>lr <Plug>(coc-rename)
nmap <silent><leader>ls :<C-u>CocFzfList symbols<CR>
vmap <silent><leader>lf <Plug>(coc-format-selected)

" Run
autocmd Filetype rust nmap <silent><leader>rB <leader>!t
    \ cargo bench -j 8<CR>
autocmd Filetype rust nmap <silent><leader>rb <leader>!t
    \ cargo build -j 8<CR>
autocmd Filetype rust nmap <silent><leader>rc <leader>!t
    \ cargo check -j 8<CR>
autocmd Filetype rust nmap <silent><leader>rC <leader>!t
    \ cargo clean -j 8<CR>
autocmd Filetype rust nmap <silent><leader>rd <leader>!t
    \ cargo doc -j 8<CR>
autocmd Filetype rust nmap <silent><leader>rr <leader>!t
    \ cargo run -j 8<CR>
autocmd Filetype rust nmap <silent><leader>rt <leader>!t
    \ cargo test -j 8<CR>
autocmd Filetype rust nmap <silent><leader>rs <leader>!t
    \ cargo search -j 8<CR>
autocmd Filetype rust nmap <silent><leader>rp <leader>!t
    \ cargo publish -j 8<CR>
autocmd Filetype rust nmap <silent><leader>rI <leader>!t
    \ cargo install -j 8<CR>
autocmd Filetype rust nmap <silent><leader>rU <leader>!t
    \ cargo uninstall -j 8<CR>

" System
nmap <leader>Spi :PlugInstall<CR>
nmap <leader>Spu :PlugUpdate<CR>

" Tweaks
nmap <leader>C ggdG<CR>
nmap <leader>q :q<CR>
nmap <leader>w :w<CR>
nmap <leader>W :w !sudo tee % > /dev/null<CR>
nmap <leader>x :x<CR>
nmap <leader>oh :CtrlP<CR>
nmap <leader>ot :CtrlP<CR>
nmap <leader>ov :CtrlP<CR>
imap <silent><expr> <c-space> coc#refresh()
nmap <silent><leader>v :CocCommand workspace.showOutput<CR>
nmap <silent><leader>V :CocRestart<CR>


" Toggle
nmap <leader>tt :TagbarToggle<CR>
nmap <leader>tn :NERDTreeToggle<CR>
nmap <c-o> :NERDTreeToggle<CR>

" Windows Tagbar FIX

" Windows
nmap <leader><TAB> :call <SID>windowJump(winnr() + 1)<CR>
nmap <leader><S-TAB> :call <SID>windowJump(winnr() - 1)<CR>
nmap <leader>1 :call <SID>windowJump(1)<CR>
nmap <leader>2 :call <SID>windowJump(2)<CR>
nmap <leader>3 :call <SID>windowJump(3)<CR>
nmap <leader>4 :call <SID>windowJump(4)<CR>
nmap <leader>5 :call <SID>windowJump(5)<CR>
nmap <leader>6 :call <SID>windowJump(6)<CR>
nmap <leader>7 :call <SID>windowJump(7)<CR>
nmap <leader>8 :call <SID>windowJump(8)<CR>
nmap <leader>9 :call <SID>windowJump(9)<CR>

" Window resize
nmap <S-Left> :vertical resize -5<CR>
nmap <S-Right> :vertical resize +5<CR>
nmap <S-Up> :resize +5<CR>
nmap <S-Down> :resize -5<CR>

" Windows manipulation
nmap <leader>ss :split<CR>
nmap <leader>sS :split +bp<CR>
nmap <leader>sv :vsplit<CR>
nmap <leader>sV :vsplit +bp<CR>
nmap <leader>st :tabnew<CR>
nmap <leader>so :only<CR>
nmap <leader>sQ :close<CR>
nmap <leader>s<TAB> :next<CR>
nmap <leader>s<S-TAB> :prev<CR>
























:tnoremap <Esc> <C-\><C-n>
