"""""""""""""""""""""""""""""""""""""
" Plugins
"""""""""""""""""""""""""""""""""""""
call plug#begin('~/.local/share/nvim/plugged')

" Utility
"Plug 'wakatime/vim-wakatime'
Plug 'haya14busa/incsearch.vim'

"Plug 'ervandew/supertab'
"Plug 'wesQ3/vim-windowswap'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'godlygeek/tabular'
"Plug 'benmills/vimux'
"Plug 'jeetsukumaran/vim-buffergator'
"Plug 'gilsondev/searchtasks.vim'
"Plug 'tpope/vim-dispatch'
"Plug 'jceb/vim-orgmode'
"Plug 'tpope/vim-speeddating'
Plug 'kerkmann/vim-header'

" Latex
Plug 'vim-latex/vim-latex'

" Generic Programming Support 
"Plug 'jakedouglas/exuberant-ctags'

" Window
Plug 'mhinz/vim-startify'
Plug 'scrooloose/nerdtree'
Plug 'Nopik/vim-nerdtree-direnter'
Plug 'majutsushi/tagbar'
Plug 'vim-airline/vim-airline'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'liuchengxu/vim-which-key'
"Plug 'ap/vim-buftabline'

" Git
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'zivyangll/git-blame.vim'

" Autocomplete
"Plug 'dense-analysis/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc-snippets'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'jiangmiao/auto-pairs'
Plug 'sheerun/vim-polyglot'
" LSP
Plug 'neoclide/coc-css'
Plug 'iamcco/coc-flutter'
Plug 'neoclide/coc-git'
Plug 'clangd/coc-clangd'
Plug 'josa42/coc-go'
Plug 'neoclide/coc-highlight'
Plug 'neoclide/coc-html'
Plug 'neoclide/coc-java'
Plug 'neoclide/coc-json'
Plug 'fannheyward/coc-markdownlint'
Plug 'yatli/coc-omnisharp'
Plug 'neoclide/coc-python'
Plug 'neoclide/coc-r-lsp'
Plug 'neoclide/coc-rls'
Plug 'fannheyward/coc-texlab'
Plug 'neoclide/coc-tsserver'
Plug 'iamcco/coc-vimlsp'
Plug 'fannheyward/coc-xml'
Plug 'neoclide/coc-yaml'
Plug 'voldikss/coc-cmake'
Plug 'marlonfan/coc-phpls'
Plug 'ckipp01/coc-metals'
Plug 'lervag/vimtex'

Plug 'iamcco/coc-actions'
" fzf
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'junegunn/fzf', {'dir': '~/.fzf','do': './install --all'}
Plug 'junegunn/fzf.vim' " needed for previews
Plug 'antoinemadec/coc-fzf'
"Plug 'neoclide/coc-vimtex'

"Plug 'neoclide/coc-spell-checker'

" Themes
Plug 'dracula/vim'
Plug 'tomasr/molokai'
Plug 'ryanoasis/vim-devicons'
Plug 'berdandy/ansiesc.vim'

"Plug 'Townk/vim-autoclose'
"Plug 'tomtom/tcomment_vim'
"Plug 'tobyS/vmustache'
"Plug 'janko-m/vim-test'
"Plug 'maksimr/vim-jsbeautify'
"Plug 'vim-syntastic/syntastic'
"Plug 'neomake/neomake'

call plug#end()
