autocmd BufWritePost *.tex call Tex_RunLaTeX()
let g:Tex_MultipleCompileFormats = 'pdf'
let g:Tex_DefaultTargetFormat = 'pdf'
