let g:NERDTreeIgnore = ['^build$']

autocmd FileType nerdtree nmap <buffer> <Left> u
autocmd FileType nerdtree nmap <buffer> <S-Left> U
autocmd FileType nerdtree nmap <buffer> <Right> <cr>
autocmd FileType nerdtree nmap <buffer> <2-LeftMouse> <cr>
" Nerdtree on the right side
let g:NERDTreeWinPos = 'rightbelow'
"autocmd StdinReadPre * let s:std_in=1
" Forbid to allow windows open in nerdtree window
autocmd BufEnter * if bufname('#') =~# "^NERD_tree_" && winnr('$') > 1 | b# | endif
" Close vim, if only nerdtree is open
"autocmd BufEnter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:plug_window = 'noautocmd vertical topleft new'
let NERDTreeQuitOnOpen = 1

let g:NERDTreeMapOpenInTab='<ENTER>'
let g:NERDTreeShowLineNumbers=1

let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "",
    \ "Staged"    : "",
    \ "Untracked" : "",
    \ "Renamed"   : "",
    \ "Unmerged"  : "",
    \ "Deleted"   : "",
    \ "Dirty"     : "",
    \ "Clean"     : "",
    \ "Ignored"   : "",
    \ "Unknown"   : ""
    \ }
