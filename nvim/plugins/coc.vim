augroup mygroup
    autocmd!
    autocmd FileType c,cpp,css,dart,dockerfile,go,groovy,haskell,html,java,javascript,json,kotlin,latex,less,php,python,r,rust,sh,typescript,vue,vim,xml,yaml setl formatexpr=CocAction('formatSelected')
augroup end

autocmd CursorHold * silent call CocActionAsync('highlight')
inoremap <silent><expr> <c-space> coc#refresh()

let $NVIM_COC_LOG_LEVEL = 'debug'
