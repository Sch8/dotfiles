let g:ctrlp_custom_ignore = '\v[\/](\.git|build|target)$'
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_open_new_file = 'v'
let g:ctrlp_reuse_window = 'startify'
let g:ctrlp_open_new_file = 't'
let g:ctrlp_prompt_mappings = {
    \ 'AcceptSelection("e")': [],
    \ 'AcceptSelection("t")': ['<cr>', '<2-LeftMouse>'],
\ }
let g:ctrlp_cmd = 'CtrlP'
