let g:ale_completion_enabled = 1
let g:ale_sign_error = 'ﮊ'
let g:ale_sign_warning = ''
highlight ALEWarning guibg=g:terminal_color_3
highlight ALEError guibg=g:terminal_color_1
" Show errors in statusline
let g:airline#extensions#ale#enabled = 1
