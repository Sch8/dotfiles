call which_key#register('<Space>', "g:which_key_map")


let g:which_key_map = {
    \ '<TAB>' : 'next window',
    \ '<S-TAB>' : 'prev window',
    \ '1' : 'window 1',
    \ '2' : 'window 2',
    \ '3' : 'window 3',
    \ '4' : 'window 4',
    \ '5' : 'window 5',
    \ '6' : 'window 6',
    \ '7' : 'window 7',
    \ '8' : 'window 8',
    \ '9' : 'window 9',
    \ ' ' : {
        \ 'name': '+Buffers',
        \ '1' : 'buffer 1',
        \ },
    \ }
let g:which_key_map.a = {
    \ 'name' : '+Applications',
    \ 's' : 'open start screen',
    \ }
let g:which_key_map.g = {
    \ 'name' : '+Gradle',
    \ 'b' : 'run task build',
    \ 'c' : 'run task check',
    \ 'r' : 'run task run',
    \ 'R' : 'run task bootRun',
    \ 't' : 'run task test',
    \ }
"let g:which_key_map.l = {
"    \ 'name' : '+Language',
"    \ 'F' : 'fix',
"    \ 'R' : 'find references',
"    \ 'd' : 'documentation',
"    \ 'e' : {
"        \ 'name': '+Error',
"        \ 'd' : 'diagnostics',
"        \ 'n' : 'next error',
"        \ 'p' : 'previous error',
"    \ },
"    \ 'f' : 'format',
"    \ 'g' : {
"        \ 'name': '+GoTo',
"        \ 'd' : 'definition',
"        \ 'i' : 'implementation',
"        \ 'r' : 'references',
"        \ 't' : 'type definition',
"    \ },
"    \ 'r' : 'rename',
"    \ 's' : 'search',
"    \ }
let g:which_key_map.o = {
    \ 'name' : '+Opener',
    \ 'h' : 'open file in new horizontal mode',
    \ 't' : 'open file in new tab',
    \ 'v' : 'open file in new vertical mode',
    \ }
let g:which_key_map.s = {
    \ 'name' : 'Window',
    \ 's' : 'set split',
    \ 'S' : 'Split with previous buffer',
    \ 'v' : 'set vsplit',
    \ 'V' : 'Vertically split with previous buffer',
    \ 't' : 'Open new tab',
    \ 'o' : 'Close other windows',
    \ 'Q' : 'Close current buffer',
    \ '<TAB>' : 'Next window or tab',
    \ '<S-TAB>' : 'Previous window or tab',
    \ }
let g:which_key_map.t = {
    \ 'name' : '+Toggle',
    \ 't' : 'toggle tagbar',
    \ 'n' : 'toggle nerdtree',
    \ }
let g:which_key_map.S = {
    \ 'name' : '+System',
    \ 'p' : {
        \ 'name': '+Plugin',
        \ 'i' : 'Plugin install',
        \ 'u' : 'Plugin update',
        \ },
    \ }
let g:which_key_map.q = 'close buffer'
let g:which_key_map.w = 'save buffer'
let g:which_key_map.W = 'save buffer with sudo'
let g:which_key_map.x = 'save buffer and close buffer'
