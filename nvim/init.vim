"""""""""""""""""""""""""""""""""""""
" NVIM configuration by
" Daniél Kerkmann
"""""""""""""""""""""""""""""""""""""
set encoding=utf8

"""""""""""""""""""""""""""""""""""""
" Install plugins
"""""""""""""""""""""""""""""""""""""
source ~/.config/nvim/plugins.vim

"""""""""""""""""""""""""""""""""""""
" VIM configuration
"""""""""""""""""""""""""""""""""""""
" Show linenumbers
set number relativenumber
" Show warning line at 120 characters
set colorcolumn=80,120

" Set indents
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab

" Always show statusline
set laststatus=2

" Enable mouse support
set mouse=a

" Set leader keys
let g:mapleader = ' '
let g:maplocalleader = ','

" ???
set splitright

" Set line break
set wrap
set linebreak
set showtabline=2

" Use system clipboard
set clipboard+=unnamedplus

" Theming
syntax enable
colorscheme dracula
highlight normal guibg=none ctermbg=none
highlight ColorColumn ctermbg=8

augroup Shebang
    autocmd BufNewFile *.py 0put =\"#!/usr/bin/env python\<nl># -*- coding: utf-8 -*-\<nl>\"|$
augroup END

augroup fixNerdtreeOpenInNewTab
    " Show linenumbers
    autocmd BufRead,BufNewFile * set number relativenumber
    " Show warning line at 120 characters
    autocmd BufRead,BufNewFile * set colorcolumn=80,120
augroup END



"""""""""""""""""""""""""""""""""""""
" Plugin configuration
"""""""""""""""""""""""""""""""""""""
source ~/.config/nvim/plugins/airline.vim
source ~/.config/nvim/plugins/ale.vim
source ~/.config/nvim/plugins/coc.vim
source ~/.config/nvim/plugins/ctrlp.vim
source ~/.config/nvim/plugins/fzf.vim
source ~/.config/nvim/plugins/git.vim
source ~/.config/nvim/plugins/header.vim
source ~/.config/nvim/plugins/incsearch.vim
source ~/.config/nvim/plugins/nerdtree.vim
source ~/.config/nvim/plugins/startify.vim
source ~/.config/nvim/plugins/tagbar.vim
source ~/.config/nvim/plugins/tex.vim
source ~/.config/nvim/plugins/whichkey.vim
source ~/.config/nvim/autostart.vim
source ~/.config/nvim/keybindings.vim

