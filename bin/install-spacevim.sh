#!/usr/bin/env bash

mkdir "${HOME}/.npm-packages"
npm config set prefix $HOME/.npm-packages

pip install --user python-language-server
npm i -g bash-language-server
npm i -g javascript-typescript-langserver
npm i -g vue-language-server
npm i -g vscode-css-languageserver-bin
$HOME/.cache/vimfiles/repos/github.com/Valloric/YouCompleteMe/install.py --all
